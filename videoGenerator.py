import os
import sys

imageFolder = "./Assets/screenshots/"
imageExt = ".png"
output = "video.mp4"
codec = "mpeg4"
fps = "1"
os.listdir(imageFolder)
images = [image for image in os.listdir(imageFolder) if image.endswith(imageExt)]

def main():
    # Use argumentParser for
    
    # Parametri Categorici
    # Parametri Numerici
    
    # Culori Haine

    imageStr = ""
    for i in images:
        imageStr += imageFolder +  i + " "

    command = "ffmpeg " + "-r " + fps + " -y -i " + imageStr + " -c:v " + codec + " -r 1 " + output
    print(command)
    os.system(command)

if __name__ == "__main__":
    main()