﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderingActors : MonoBehaviour
{
    public Camera[] cameras;

    public int screenshotWidth;
    public int screenshotHeight;
    public bool takePhotos;

    public string screenshotsPath = "../../screenshots";
    public string jsonsPath = "../../jsons";

    public List<string> jsonElements;

    // Start is called before the first frame update
    void Start()
    {
        if(!System.IO.Directory.Exists(Application.dataPath + "/" + screenshotsPath))
        {
            // Create screenshot folder
            System.IO.Directory.CreateDirectory(Application.dataPath + "/" + screenshotsPath);
        }

        if(!System.IO.Directory.Exists(Application.dataPath + "/" + jsonsPath))
        {
            // Create json folder
            System.IO.Directory.CreateDirectory(Application.dataPath + "/" + jsonsPath);
        }
    }

    // Update is called once per frame
    void Update()
    {
        foreach (Camera camera in cameras)
        {
            jsonElements = new List<string>();
            GetAllVisible(camera);

            if (takePhotos)
            {
                // Save the screenshot
                RenderTexture renderTexture = new RenderTexture(screenshotWidth, screenshotHeight, 24);
                camera.targetTexture = renderTexture;

                Texture2D screenshot = new Texture2D(screenshotWidth, screenshotHeight, TextureFormat.RGB24, false);
                camera.Render();

                RenderTexture.active = renderTexture;

                screenshot.ReadPixels(new Rect(0, 0, screenshotWidth, screenshotHeight), 0, 0);

                camera.targetTexture = null;
                RenderTexture.active = null;

                Destroy(renderTexture);

                byte[] bytes = screenshot.EncodeToPNG();
                string filename = ScreenShotName(screenshotWidth, screenshotHeight, camera);
                System.IO.File.WriteAllBytes(filename, bytes);

                // Print Json to file
                string jsonFilename = JsonFileName(screenshotWidth, screenshotHeight, camera);
                System.IO.File.WriteAllLines(jsonFilename, jsonElements);

                jsonElements = null;
            }
        }
    }

    private void GetAllVisible(Camera camera)
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);

        foreach (GameObject actor in GameObject.FindGameObjectsWithTag("Human"))
        {
            int componentsNumber = 0;

            foreach (SkinnedMeshRenderer subObject in actor.GetComponentsInChildren<SkinnedMeshRenderer>())
            {
                Vector3 pointOnScreen = camera.WorldToScreenPoint(subObject.bounds.center);

                if (pointOnScreen.z < 0)
                {
                    // Debug.Log("BEHIND: " + subObject.name);
                    break;
                }

                if (pointOnScreen.x < 0 || pointOnScreen.x > Screen.width || pointOnScreen.y < 0 || pointOnScreen.y > Screen.height)
                {
                    // Debug.Log("OUT OF BOUNDS: " + subObject.name);
                    break;
                }

                RaycastHit hit;

                Vector3 heading = subObject.transform.position - camera.transform.position;
                Vector3 direction = heading.normalized;

                if (Physics.Linecast(camera.transform.position, subObject.bounds.center, out hit))
                {
                    if (hit.transform.name != subObject.name)
                    {
                        // Debug.Log("OCCLUDED by " + hit.transform.name);
                        
                        // prone to errors, in cazul in care 2 personaje se suprapun
                        // pentru ca noi facem dupa nume, ar trebui sa generam fiecarei componente un id
                        // de pus cu GetInstanceID   
                        break;
                    }
                }

                ++componentsNumber;
            }

            // This will have to be edited into seeing only the bounding box of the hair
            if (takePhotos)
            {
                if (componentsNumber == 7)
                {
                    Vector2 screenPosition = camera.WorldToScreenPoint(actor.transform.position);
                    //Debug.Log(actor.name + " is visible at " + screenPosition);

                    //Person person = new Person(actor.GetInstanceID(), screenPosition.x, screenPosition.y, 10.0f, 10.0f, false, 0);

                    //PersonSerializable person = new PersonSerializable(actor.GetInstanceID(), screenPosition.x, screenPosition.y, 10.0f, 10.0f, false, 0);

                    Person component = actor.GetComponent<Person>();

                    if(component != null)
                    {
                        component.setScreenPosition(screenPosition.x, screenPosition.y);
                        string json = JsonUtility.ToJson(component);
                        jsonElements.Add(json);
                    }

                    //Debug.Log(json);
                }
            }
        }
    }

    public string ScreenShotName(int width, int height, Camera camera)
    {
        // Change later to ID
        return string.Format("{0}/{5}/screencam_{1}_res_{2}x{3}_date_{4}.png", Application.dataPath, camera.name, width, height, System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"), screenshotsPath);
    }

    public string JsonFileName(int width, int height, Camera camera)
    {
        // Change later to ID
        return string.Format("{0}/{5}/screencam_{1}_res_{2}x{3}_date_{4}.json", Application.dataPath, camera.name, width, height, System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"), jsonsPath);
    }
}