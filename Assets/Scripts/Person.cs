﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Person : MonoBehaviour
{
    public enum Emotion { angry, neutral, scared, happy, sad, surprised };

    // Will be set on start
    [SerializeField]
    [HideInInspector]
    private int id;

    [SerializeField]
    [HideInInspector]
    private float x;

    [SerializeField]
    [HideInInspector]
    private float y;

    public bool gender; // 0 for female, 1 for male
    public float height;
    public float weight;
    public Emotion emotion = Emotion.neutral;

    public void setScreenPosition(float x, float y)
    {
        this.x = x;
        this.y = y;
    }

    // Start is called before the first frame update
    void Start()
    {
        this.id = this.transform.parent.gameObject.GetInstanceID();
        GameObject managerObject = GameObject.FindWithTag("Manager");
        managerObject.GetComponent<ChooseModel>().chooseModel(this);

    }

    // Update is called once per frame
    void Update()
    {

    }
}
