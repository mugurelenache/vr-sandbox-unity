﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseModel : MonoBehaviour
{
    public GameObject[] femaleVariants;
    public GameObject[] maleVariants;
    public AnimationControllerMatch[] matches;

    public void chooseModel(Person personComponent)
    {
        if (personComponent)
        {
            float rand = 0;
            int rounded = 0;
            Transform personTransform = personComponent.transform;
            GameObject prefab;
            Animator animator;

            if (personComponent.gender)
            {
                rand = Random.value * (float)maleVariants.Length;
                rounded = Mathf.FloorToInt(rand);

                // Create object with anim & stuff
                prefab = Instantiate<GameObject>(maleVariants[rounded], personTransform.position, new Quaternion(personTransform.rotation.x, personTransform.rotation.y - 90, personTransform.rotation.z, personTransform.rotation.w)) as GameObject;
            }
            else
            {
                rand = Random.value * (float)femaleVariants.Length;
                rounded = Mathf.FloorToInt(rand);

                // Create object with anim & stuff
                prefab = Instantiate<GameObject>(femaleVariants[rounded], personTransform.position, new Quaternion(personTransform.rotation.x, personTransform.rotation.y - 90, personTransform.rotation.z, personTransform.rotation.w)) as GameObject;
            }

            if (prefab)
            {
                prefab.transform.localScale *= 0.5f;

                // Randomize for animation controller
                rand = Random.value * (float)matches.Length;
                rounded = Mathf.FloorToInt(rand);

                // Setting up the animator
                animator = prefab.GetComponent<Animator>();
                animator.applyRootMotion = true;
                animator.updateMode = AnimatorUpdateMode.Normal;
                animator.cullingMode = AnimatorCullingMode.CullUpdateTransforms;

                // We need to set the avatar in the animator to match the animation
                animator.avatar = matches[rounded].avatar;

                // We need to set the anim
                animator.runtimeAnimatorController = matches[rounded].runtimeAnimator;

                Dictionary<string, SkinnedMeshRenderer> blendShapes = new Dictionary<string, SkinnedMeshRenderer>();

                Transform[] humanComponents = prefab.transform.GetComponentsInChildren<Transform>(true);

                foreach (Transform child in humanComponents)
                {
                    blendShapes.Add(child.name, child.GetComponent<SkinnedMeshRenderer>());
                }

                // Set emotions
                setEmotion(personComponent, prefab, Person.Emotion.angry, blendShapes);

                // Sets clothes color using different parameters (minHue, maxHue, minSaturation, maxSaturation)
                setColor(blendShapes["Tops"], 0, 1, 0, 1);
                setColor(blendShapes["Bottoms"], 0, 1, 0, 1);
                setColor(blendShapes["Shoes"], 0, 1, 0, 1);
                setColor(blendShapes["Hair"], 0, 1, 0.5f, 1);

                // Sets hats color (need to try-catch as it is more efficient than 'containsKey')
                try
                {
                    setColor(blendShapes["Hats"], 0, 1, 0, 1);
                }
                catch (KeyNotFoundException)
                {
                    // Nothing
                }

                // Set height
                // TODO
                // Set weight
                // TODO

                // Attach to parent -- we need to ensure that the spawner clearly has the 'Human' tag
                prefab.transform.SetParent(personComponent.transform);
            }
        }
    }

    public void setEmotion(Person personComponent, GameObject prefab, Person.Emotion emotion, Dictionary<string, SkinnedMeshRenderer> blendShapes)
    {
        personComponent.emotion = emotion;

        switch (emotion)
        {
            case Person.Emotion.angry:
                blendShapes["Body"].SetBlendShapeWeight(32, 0);
                blendShapes["Body"].SetBlendShapeWeight(33, 0);
                break;
            case Person.Emotion.neutral:
                break;
            case Person.Emotion.scared:
                break;
            case Person.Emotion.happy:
                break;
            case Person.Emotion.sad:
                break;
            case Person.Emotion.surprised:
                break;
            default:
                personComponent.emotion = Person.Emotion.neutral;
                break;
        }
    }

    public void setWeight(Person personComponent, GameObject prefab, Dictionary<string, SkinnedMeshRenderer> blendShapes, float minWeight, float maxWeight)
    {
        float weight = Random.Range(minWeight, maxWeight);
        personComponent.weight = weight;

        // Need blendshape to change weight
    }

    public void setHeight(Person personComponent, GameObject prefab, Dictionary<string, SkinnedMeshRenderer> blendShapes, float minHeight, float maxHeight)
    {
        float height = Random.Range(minHeight, maxHeight);
        personComponent.height = height;

        // Need blendshape to change height
    }

    // Sets the tops color
    public void setColor(SkinnedMeshRenderer blendShape, float minHue, float maxHue, float minSat, float maxSat)
    {
        blendShape.materials[0].SetColor("_Color", Random.ColorHSV(minHue, maxHue, minSat, maxSat));
    }
}
