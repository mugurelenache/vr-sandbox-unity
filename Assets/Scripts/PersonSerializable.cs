﻿[System.Serializable]
public class PersonSerializable
{
    public int id;

    public float x;
    public float y;

    public float height;
    public float weight;

    public bool gender;

    public int emotion;

    public PersonSerializable(int id, float x, float y, float height, float weight, bool gender, int emotion)
    {
        this.id = id;
        this.x = x;
        this.y = y;
        this.height = height;
        this.weight = weight;
        this.gender = gender;
        this.emotion = emotion;
    }
}
