﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AnimationControllerMatch
{
    public RuntimeAnimatorController runtimeAnimator;
    public Avatar avatar;

    public AnimationControllerMatch(RuntimeAnimatorController runtimeAnimator, Avatar avatar)
    {
        this.runtimeAnimator = runtimeAnimator;
        this.avatar = avatar;
    }

    public AnimationControllerMatch() { }
}
