﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneSpawner : MonoBehaviour
{
    public GameObject objectToInstantiate;
    public Vector3 startPosition = new Vector3(0.0f, 0.0f, 0.0f);
    public int nTiles = 0;
    public int mTiles = 0;
    public int xOffset = 5;
    public int zOffset = 10;
    public Material carPaint;
    public GameObject[] instances;

    // Start is called before the first frame update
    void Start()
    {
        instances = new GameObject[nTiles * mTiles];

        objectToInstantiate.transform.Rotate(new Vector3(1.0f, 0.0f, 0.0f), 0.0f);

        Material carPaint = objectToInstantiate.GetComponent<Renderer>().sharedMaterials[0] as Material;

        makeInvisible(objectToInstantiate);

        for (int i = 0; i < nTiles; i++)
        {
            startPosition.x = 0;
            startPosition.z = i * zOffset;

            for (int j = 0; j < mTiles; j++)
            {
                int pos = i * mTiles + j;
                startPosition.x = j * xOffset;

                // Check to see whether is on border
                instances[pos] = Instantiate(objectToInstantiate, startPosition, Quaternion.Euler(-90.0f, 0.0f, 0.0f)) as GameObject;
                instances[pos].GetComponent<Renderer>().materials[0].SetColor("_Color", Random.ColorHSV());

                // If it is on the top border, spawn building on top
                if (i == 0)
                {
                    if (j == 0)
                    {
                        makeVisibleAndRotateBuilding(instances[pos], true, new Vector3(0.0f, 0.0f, 0.0f));
                        makeVisibleAndRotateBuilding(instances[pos], false, new Vector3(0.0f, 0.0f, 90.0f));
                    }
                    else if(j == mTiles - 1)
                    {
                        makeVisibleAndRotateBuilding(instances[pos], false, new Vector3(0.0f, 0.0f, 0.0f));
                        makeVisibleAndRotateBuilding(instances[pos], true, new Vector3(0.0f, 0.0f, 0.0f));
                    }
                    else
                    {
                        makeVisibleAndRotateBuilding(instances[pos], false, new Vector3(0.0f, 0.0f, 0.0f));
                    }
                }
                // If it is on the bottom border, spawn building on top and rotate mesh
                else if(i == nTiles - 1)
                {
                    if (j == 0)
                    {
                        makeVisibleAndRotateBuilding(instances[pos], true, new Vector3(0.0f, 0.0f, 0.0f));
                        makeVisibleAndRotateBuilding(instances[pos], false, new Vector3(0.0f, 0.0f, 180.0f));
                    }
                    else if (j == mTiles - 1)
                    {
                        makeVisibleAndRotateBuilding(instances[pos], false, new Vector3(0.0f, 0.0f, 0.0f));
                        makeVisibleAndRotateBuilding(instances[pos], true, new Vector3(0.0f, 0.0f, -90.0f));
                    }
                    else
                    {
                        makeVisibleAndRotateBuilding(instances[pos], false, new Vector3(0.0f, 0.0f, 180.0f));
                    }
        
                }
                // If it is on the left border, spawn building on left
                else if(j == 0)
                {
                    makeVisibleAndRotateBuilding(instances[pos], true, new Vector3(0.0f, 0.0f, 180.0f));
                }
                // If it is on the right border, spawn building on left and rotate mesh
                else if(j == mTiles - 1)
                {
                    makeVisibleAndRotateBuilding(instances[pos], true, new Vector3(0.0f, 0.0f, 0.0f));
                }
            }
        }
    }

    // Building 0 = A01
    // Building 1 = A02
    void makeVisibleAndRotateBuilding(GameObject inst, bool building, Vector3 rotation)
    {
        inst.transform.Rotate(rotation);

        for (int i = 0; i < inst.transform.childCount; i++)
        {
            if (!building && inst.transform.GetChild(i).name.Equals("Building_A01"))
            {
                // Do stuff
                inst.transform.GetChild(i).gameObject.SetActive(true);
                return;

            }
            else if (building && inst.transform.GetChild(i).name.Equals("Building_A02"))
            {
                // Do stuff
                inst.transform.GetChild(i).gameObject.SetActive(true);
                return;
            }
        }
    }


    void makeInvisible(GameObject inst)
    {
        for(int i = 0; i < inst.transform.childCount; i++)
        {
            if (inst.transform.GetChild(i).name.Equals("Building_A01") || inst.transform.GetChild(i).name.Equals("Building_A02"))
            {
                inst.transform.GetChild(i).gameObject.SetActive(false);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
